= two

Here is chapter 2.

xrefstyle = {xrefstyle}

* Plain xref without an anchor
** xref:chapter-1.adoc[]
* `xrefstyle=basic`
** xref:chapter-1.adoc[xrefstyle=basic]
* `xrefstyle=short`
** xref:chapter-1.adoc[xrefstyle=short]
* `xrefstyle=full`
** xref:chapter-1.adoc[xrefstyle=full]

* With _one_ anchor and `xrefstyle=basic`:
** xref:chapter-1.adoc#_one_[xrefstyle=basic]
* * With _one_ anchor and `xrefstyle=short`:
** xref:chapter-1.adoc#_one_[xrefstyle=short]
* With _one_ anchor and `xrefstyle=full`
** xref:chapter-1.adoc#_one_[xrefstyle=full]

* With _one anchor and `xrefstyle=basic`:
** xref:chapter-1.adoc#_one[xrefstyle=basic]
* * With _one anchor and `xrefstyle=short`:
** xref:chapter-1.adoc#_one[xrefstyle=short]
* With _one anchor and `xrefstyle=full`
** xref:chapter-1.adoc#_one[xrefstyle=full]

* With _one_ anchor and `xrefstyle=basic`:
** xref:chapter-1.adoc#_one_[xrefstyle=basic]
* * With _one_ anchor and `xrefstyle=short`:
** xref:chapter-1.adoc#_one_[xrefstyle=short]
* With _one_ anchor and `xrefstyle=full`
** xref:chapter-1.adoc#_one_[xrefstyle=full]

* With _one anchor and `xrefstyle=basic`:
** xref:chapter-1.adoc#_one[xrefstyle=basic]
* * With _one anchor and `xrefstyle=short`:
** xref:chapter-1.adoc#_one[xrefstyle=short]
* With _one anchor and `xrefstyle=full`
** xref:chapter-1.adoc#_one[xrefstyle=full]

* With _two_ anchor and `xrefstyle=basic`:
** xref:chapter-2.adoc#_two_[xrefstyle=basic]
* * With _two_ anchor and `xrefstyle=short`:
** xref:chapter-2.adoc#_two_[xrefstyle=short]
* With _two_ anchor and `xrefstyle=full`
** xref:chapter-2.adoc#_two_[xrefstyle=full]

* With _two anchor and `xrefstyle=basic`:
** xref:chapter-2.adoc#_two[xrefstyle=basic]
* * With _two anchor and `xrefstyle=short`:
** xref:chapter-2.adoc#_two[xrefstyle=short]
* With _two anchor and `xrefstyle=full`
** xref:chapter-2.adoc#_two[xrefstyle=full]